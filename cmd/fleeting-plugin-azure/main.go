package main

import (
	"flag"
	"fmt"
	"os"

	azure "gitlab.com/gitlab-org/fleeting/fleeting-plugin-azure"
	"gitlab.com/gitlab-org/fleeting/fleeting/plugin"
)

var (
	showVersion = flag.Bool("version", false, "Show version information and exit")
)

func main() {
	flag.Parse()
	if *showVersion {
		fmt.Println(azure.Version.Full())
		os.Exit(0)
	}

	plugin.Serve(&azure.InstanceGroup{})
}
